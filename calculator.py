#!/usr/bin/env python3

# Basic minimalist calculator

from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/api/compute', methods=['GET', 'POST'])
def compute():
    print('compute called')
    parameter1 = int(request.args.get('parameter1'))
    parameter2 = int(request.args.get('parameter2'))
    result = parameter1 * parameter2
    return jsonify({'result': result}), 200

@app.route('/', methods=['GET', 'POST'])
def default_page():
    print('default_page called')
    ret = """
    <!doctype html>
    <html>
    <title>Calculator</title>
    <body>
    Parameter 1: <input type="text" id="parameter1" value="3"/> <br>
    Parameter 2: <input type="text" id="parameter2" value="4"/> <br>
    <input type="button" value="multiply" onclick="compute()"/> <br>
    <div id="result"></div>
    <script>
    function compute() {
      let parameter1 = document.getElementById("parameter1").value;
      let parameter2 = document.getElementById("parameter2").value;
      fetch('api/compute?parameter1=' + parameter1 + "&parameter2=" + parameter2)
        .then(function(response) {
          return response.json();
        })
        .then(function(myJson) {
          document.getElementById("result").innerHTML = " Result=" + myJson['result'];
      });
 
    }
    </script>
    </body>
    </html>
    """
    return ret

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)
