# Introduction

This project shows a basic usage of flask.
This example consists of a simple calculator.

# Pre-requesite

pip3 install flask

# Launch

python3 calculator.py

Then open the url: http://localhost:8080

# Caveat

The port 8080 must be available on localhost.
